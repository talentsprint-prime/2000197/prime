package ecc;

public class ECC_82_AdamNumber {

	public static void main(String[] args) {
		int num = 311;
		System.out.println(isAdamNumber(num));
	}

	// return true if the given number is an AdamNumber
	public static boolean isAdamNumber(int num) {

		int a = getSquare(num);
		int b = getSquare(getReverse(num));
		// System.out.println("a "+a+" b "+b);

		if (a == getReverse(b))

			return true;

		return false;

		// ADD YOUR CODE HERE
	}

	// return the reverse of the given number
	public static int getReverse(int n) {

		int reverse = 0;

		while (n > 0) {

			int remainder = n % 10;
			reverse = reverse * 10 + remainder;
			n /= 10;
		}
		// System.out.println(reverse);
		return reverse;
		// ADD YOUR CODE HERE
	}

	// return the square of the give number
	public static int getSquare(int n) {
		// System.out.println(n*n);

		return n * n;
	}
}
