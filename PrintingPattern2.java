package ecc;

import java.util.Scanner;

public class PrintingPattern2 {

	public static void main (String[] args){

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();

		getPattern(num);
	}

	public static void getPattern(int num){


		for(int i=1;i<=num;i++)
		{
			for(int j=num;j>i;j--){

				System.out.print("1 ");
			}
			for(int k=1;k<=i;k++){

				System.out.print(k +" ");
			}
			System.out.println();
		}
	}

}
