package ecc;

public class Armstrong01 {

	public static void main(String[] args) {
		int num1 = 100;
		int num2 = 125;
		System.out.println(generateArmstrongNums(num1, num2));
	}

	public static String generateArmstrongNums(int start, int limit) {
		
		String result = "";
    
    	if( start <= 0 || limit <= 0)
    		
    		return "-1";
    	
    	else if(start >= limit)
    		
    		return "-2";
    	else{
    		
    		for(int i = start; i < limit; i++){
    			
    			if(isArmstrong(i))
    				result = result + i +",";
    			
    		}
    		
    	}
    	
    	if(result.isEmpty())
    		return "-3";
    	
    	return result.substring(0, result.length()-1);
    	
    	
        //ADD YOUR CODE HERE
    }

    public static boolean isArmstrong(int num) {
    	
    	return num == sumOfPowersOfDigits(num);
        //ADD YOUR CODE HERE
    }

    public static int sumOfPowersOfDigits(int n) {
    	
    	int temp = n, sum =0;
    	
    	int digits[] = getDigits(n);
    	
    	for(int i = 0; i < digits.length; i++){
    		
    		int powervalue = power(digits[i],digits.length);
    		
    		sum = sum + powervalue;
    	}
    	return sum;
    	//ADD YOUR CODE HERE
    }

    public static int[] getDigits(int n) {
    	
    	int temp = n , digitCount = 0;
    	
    	while(temp > 0){
    		
    		digitCount++;
    		
    		temp = temp/10;
    		
    	}
    	
    	int digits[] = new int[digitCount];
    	
    	int i = 0;
    	
    	while(n>0){
    		
    		int digit = n%10;
    		
    		digits[i]=digit;
    		
    		i++;
    		
    		n/=10;
    	}
    	return digits;
    	
    	
    }
        //ADD YOUR CODE HERE
    

	public static int power(int d, int p) {

		double pow = Math.pow(d, p);

		int power = (int) pow;

		return power;

	}

}
