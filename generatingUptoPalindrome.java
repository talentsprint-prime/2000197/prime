package ecc;

import java.util.Scanner;

public class generatingUptoPalindrome {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();

		getNextPalindrome(num);
	}

	public static int reverseOfNumber(int num) {

		int reverse = 0;

		while (num > 0) {

			int remainder = num % 10;

			reverse = reverse * 10 + remainder;

			num /= 10;

		}

		return reverse;
	}

	public static boolean isPalindrome(int num) {

		return (num == reverseOfNumber(num));
	}

	public static void getNextPalindrome(int num) {

		while (num > 0) {

			num = reverseOfNumber(num) + num;

			if (isPalindrome(num)) {

				System.out.println(num);

				break;
			}

			else
				continue;
		}

	}
}
