package ecc;

import java.util.Scanner;

public class SearchForAnElement {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements");

		for (int i = 0; i < arr.length; i++) {

			arr[i] = sc.nextInt();
		}

		System.out.println("Elements in the array are :");

		for (int i = 0; i < arr.length; i++) {

			System.out.println(arr[i]);
		}
		System.out.println("Enter key value :");

		int key = sc.nextInt();

		System.out.println(isElementEqualToKey(arr, key));
	}

	public static boolean isElementEqualToKey(int[] arr, int key) {

		for (int i = 0; i < arr.length; i++) {

			if (key == arr[i])

				return true;

		}
		return false;
	}
}
