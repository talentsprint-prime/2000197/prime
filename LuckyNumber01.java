package ecc;

public class LuckyNumber01 {

	public static void main(String[] args) {

		String date = "15-March-2016";
		System.out.println(getLuckyNumber(date));
	}

	public static int getLuckyNumber(String date) {

		String[] dateParts = date.split("-");

		int dd = Integer.parseInt(dateParts[0]);
		int mon = convertMMMtoMM(dateParts[1]);
		int year = Integer.parseInt(dateParts[2]);

		int sum_of_digits = getSumOfDigits(dd);
		sum_of_digits += getSumOfDigits(mon);
		sum_of_digits += getSumOfDigits(year);

		while (sum_of_digits > 10) {
			sum_of_digits = getSumOfDigits(sum_of_digits);

		}
		return sum_of_digits;

		// ADD YOUR CODE HERE
	}

	public static int convertMMMtoMM(String mon) {

		String months = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";

		mon = mon.substring(0, 3);
		mon = mon.toUpperCase();

		return ((months.indexOf(mon) / 3) + 1);

		// ADD YOUR CODE HERE
	}

	public static int getSumOfDigits(int num) {

		int sum = 0;
		while (num > 0) {

			sum = sum + num%10;;
			num = num / 10;
		}
		return sum;
		// ADD YOUR CODE HERE
	}

}
