package ecc;

import java.util.Scanner;

public class ChessBoard1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int chessBoard[][] = new int[8][8];

		System.out.println("Enter x-axis position");

		int x_position = sc.nextInt();

		System.out.println("Enter y-axis position");

		int y_position = sc.nextInt();

		findNoOfPossibleMoves(chessBoard, x_position, y_position);
	}

	public static void findNoOfPossibleMoves(int chessBoard[][], int x_position, int y_position) {

		int x[] = { -1, 1, 2, 2, -2, -2, -1, 1 };

		int y[] = { 2, 2, -1, 1, 1, -1, -2, -2 };

		int count = 0;

		System.out.println("The possible positions are ");

		for (int i = 0; i < 8; i++) {

			int p = x_position + x[i];
			int q = y_position + y[i];

			if (p >= 0 && q >= 0 && p < 8 && q < 8) {

				count++;
			}

			System.out.print("(" + p + "," + q + ")" + " ");
		}
		System.out.println();

		System.out.println("No.of possible moves for given position is " + count);

	}

}
